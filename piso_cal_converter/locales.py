# -*- coding: utf-8 -*-

"""
locales.py: Used for formatting datetime stamps in language specific ways.

Python module locales is not used because Heroku lacks swedish as locale.
"""


import datetime

_lang_table = {
    'sv_SE': {
        'weekdays_full': ['måndag', 'tisdag', 'onsdag', 'torsdag',
                          'fredag', 'lördag', 'söndag'],
        'weekdays_abbr': ['mån', 'tis', 'ons', 'tors', 'fre', 'lör', 'sön'],
        'month_full': ['januari', 'februari', 'mars', 'april', 'maj', 'juni',
                       'juli', 'augusti', 'september', 'oktober', 'november',
                       'december'],
        'month_abbr': ['jan', 'feb', 'mars', 'apr', 'maj', 'juni', 'juli',
                       'aug', 'sept', 'okt', 'nov', 'dec'],
        'clock_full': 'klockan',
        'clock_abbr': 'kl'
    }
}


class LocalizedDatetime():
    def __init__(self, dt, locale='sv_SE'):
        assert isinstance(dt, datetime.date) or isinstance(dt, datetime.datetime)
        self._datetime = dt
        try:
            self._table = _lang_table[locale]
        except KeyError:
            raise ValueError('Locale not supported')

    def time(self, *args, **kwargs):
        if hasattr(self._datetime, 'time'):
            return self._datetime.time(*args, **kwargs)

    @property
    def day(self):
        return self._datetime.day

    @property
    def month_full(self):
        return self._table['month_full'][self._datetime.month - 1]

    @property
    def month_abbr(self):
        return self._table['month_abbr'][self._datetime.month - 1]

    @property
    def timestamp(self):
        if self.time:
            return '{}:{}'.format(self.time().hour, self.time().minute)

    @property
    def year(self):
        return self._datetime.year

    @property
    def weekday_full(self):
        return self._table['weekdays_full'][self._datetime.isoweekday() - 1]

    @property
    def weekday_abbr(self):
        return self._table['weekdays_abbr'][self._datetime.isoweekday() - 1]

    # CUSTOM STYLES

    # AKA "Programblad"
    @property
    def style1_date(self):
        return '{} {} {}'.format(self.weekday_full.capitalize(), self.day, self.month_full)

    @property
    def style1_datetime(self):
        # Can't show time if instance is date and not datetime, thus show date-style
        if isinstance(self._datetime, datetime.datetime):
            return '{} {} {} {} {}:{:02d}'.format(
                self.weekday_full.capitalize(), self.day, self.month_full, self._table['clock_abbr'],
                self.time().hour, self.time().minute
            )

        return self.style1_date
