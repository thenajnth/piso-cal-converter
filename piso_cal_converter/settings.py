# -*- coding: utf-8 -*-

from pytz import timezone
import os.path

BASE_DIR = os.path.abspath(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
EXPORT_TEMPLATES_DIR = os.path.join(BASE_DIR, 'export_templates')
TEMPLATES_DIR = os.path.join(BASE_DIR, 'templates')

SVG_DEFAULT_TEMPLATE = 'generic1'
SVG_YEAR_TEMPLATE = '{}'
SVG_WEEK_TEMPLATE = 'Vecka {}'

VIDEO_DEFAULT_TEMPLATE = 'brand-with-footer'

STATIC_DIR = os.path.join(BASE_DIR, 'static')

UI_URL_TEMPLATE = '/ui/{user_key}/'
DOC_URL_TEMPLATE = 'https://docs.google.com/document/d/{file_id}/export?format={fmt}'

LOCALE = 'sv_SE'
TZ = timezone('Europe/Stockholm')
