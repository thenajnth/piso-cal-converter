import os.path
import subprocess
import tempfile

from io import BytesIO

from piso_cal_converter.image_export import gen_week_schedule_png
from piso_cal_converter.tools import get_export_templates


def gen_week_schedule_video(items, year, week_no, template_name):
    _templates = get_export_templates('video')
    template = _templates[template_name]

    stream_in, _ = gen_week_schedule_png(items, year, week_no, template_name, 'video')

    f_png = tempfile.NamedTemporaryFile(mode='w+b', suffix='.png',
                                        prefix='piso_cal_export_png_')
    f_png.write(stream_in.read())

    with tempfile.TemporaryDirectory() as output_dir:
        background_path = template.video_fullpath
        overlay_path = f_png.name
        output_path = os.path.join(output_dir, 'video.mp4')

        cmd = ['ffmpeg', '-v', 'quiet', '-stats', '-y', '-i', background_path,
               '-i', overlay_path, '-filter_complex', 'overlay[out]', '-map',
               '[out]', output_path]

        subprocess.run(cmd)

        with open(output_path, 'rb') as f:
            stream_out = BytesIO(f.read())

    f_png.close()

    mimetype = 'video/mp4'

    return stream_out, mimetype
