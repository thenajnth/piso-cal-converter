# -*- coding: utf-8 -*-

import argparse

from collections import OrderedDict
from datetime import datetime, date
from os import listdir
from os.path import isfile, join

from piso_cal_converter import settings
from piso_cal_converter.templateclasses import SvgTemplate, VideoTemplate


def events_by_weekno(events):
    weeks = OrderedDict()

    for event in events:
        weekno = event.weekno

        if weekno in weeks:
            weeks[weekno].append(event)
        else:
            weeks[weekno] = [event]

    return weeks


def events_by_date(events):
    datestamps = OrderedDict()

    for event in events:
        datestamp = event.start.strftime('%Y-%m-%d')

        if datestamp in datestamps:
            datestamps[datestamp].append(event)
        else:
            datestamps[datestamp] = [event]

    return datestamps


def get_events_from_items(items, start_date=None, end_date=None):
    events = [item
              for item in items
              if getattr(item, 'category', None) == 'event']

    new_list = []

    if not any([start_date, end_date]):
        new_list = events

    elif all([start_date, end_date]):
        assert isinstance(start_date, date)
        assert isinstance(end_date, date)

        for event in events:
            if isinstance(event.start, datetime):
                event_start_date = event.start.date()
            else:
                event_start_date = event.start

            if event_start_date <= end_date and event_start_date >= start_date:
                new_list.append(event)

    elif start_date:
        assert isinstance(start_date, date)

        for event in events:
            if isinstance(event.start, datetime):
                event_start_date = event.start.date()
            else:
                event_start_date = event.start

            if event_start_date >= start_date:
                new_list.append(event)

    elif end_date:
        for event in events:
            if isinstance(event.start, datetime):
                event_start_date = event.start.date()
            else:
                event_start_date = event.start

            if event_start_date <= end_date:
                new_list.append(event)

    return new_list


def get_export_templates(template_type):
    assert(template_type in ['video', 'svg'])
    path = join(settings.EXPORT_TEMPLATES_DIR, template_type)
    templates = OrderedDict()
    template_names = (name
                      for name in listdir(path)
                      if isfile(join(template_full_path(template_type, name), 'config.json')))

    for name in sorted(template_names):
        if template_type == 'video':
            templates[name] = VideoTemplate(name)
        elif template_type == 'svg':
            templates[name] = SvgTemplate(name)
        else:
            raise TypeError("template_type must be 'video' or 'svg'")
    return templates


def valid_date(s, none_on_error=False):
    """
    Primary use as type in argparse to convert iso8601 strings to datetime objects.
    But can also be used as generic iso8601-converter and return None on errors with
    argument none_on_error set to True.
    """
    if not s:
        return None

    try:
        return datetime.strptime(s, "%Y-%m-%d").date()
    except ValueError:
        if none_on_error:
            return None

        msg = "Felaktigt datumformat: '{0}'. Ange datum som YYYY-MM-DD.".format(s)
        raise argparse.ArgumentTypeError(msg)


def template_full_path(template_type, template_name):
    assert(template_type in ['video', 'svg'])
    type_path = join(settings.EXPORT_TEMPLATES_DIR, template_type)
    return join(type_path, template_name)
