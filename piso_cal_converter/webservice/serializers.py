# -*- coding: utf-8 -*-

import json

from piso_cal_converter.tools import get_events_from_items
from piso_cal_converter.validation import run_all_checks
from piso_cal_converter.webservice.tools import json_serial

def _events(items, start_date=None, end_date=None):
    events = get_events_from_items(items, start_date=start_date, end_date=end_date)
    return [i.get_dict() for i in events]


def _validation(items):
    return [i.get_dict() for i in run_all_checks(items)]


def get_events(items, start_date=None, end_date=None):
    data = _events(items, start_date=start_date, end_date=end_date)
    return json.dumps(data, default=json_serial)


def get_validation(items):
    data = _validation(items)
    return json.dumps(data)


def get_validation_and_events(items, start_date=None, end_date=None):
    data = {
        'validation': _validation(items),
        'events': _events(items, start_date=start_date, end_date=end_date)
    }
    return json.dumps(data, default=json_serial)
