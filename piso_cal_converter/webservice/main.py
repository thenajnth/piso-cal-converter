#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

import jinja2

from flask import Flask, abort, make_response, redirect, request
from whitenoise import WhiteNoise

from piso_cal_converter import export
from piso_cal_converter.parser import parse_docx
from piso_cal_converter.settings import STATIC_DIR, SVG_DEFAULT_TEMPLATE
from piso_cal_converter.settings import TEMPLATES_DIR, UI_URL_TEMPLATE
from piso_cal_converter.image_export import gen_week_schedule, gen_week_schedule_png
from piso_cal_converter.tools import get_export_templates, valid_date
from piso_cal_converter.validation.helpers import jsonify
from piso_cal_converter.validation import run_all_checks
from piso_cal_converter.webservice.tools import check_user_key
from piso_cal_converter.webservice import serializers


FILE_ID = os.environ.get('PLANNING_FILE_ID')

app = Flask(__name__)
app.wsgi_app = WhiteNoise(app.wsgi_app, root=STATIC_DIR)

jinja2_env = jinja2.Environment(loader=jinja2.FileSystemLoader(TEMPLATES_DIR))


def _get_week_schedule_args(request):
    # Year
    year = request.args.get('year')
    try:
        year = int(year)
    except ValueError:
        print(1)
        app.logger.error('`{}` is not valid year for `{}`.'.format(
            request.args.get('year'), request.path))
        abort(500)
    except TypeError:
        print(2)
        app.logger.error('Argument `year` must be present in `{}`'.format(request.path))
        abort(500)

    # Week no
    try:
        week_no = int(request.args.get('week_no'))
    except ValueError:
        print(3)
        app.logger.error('`{}` is not a valid week no for `{}`.'.format(request.path))
        abort(500)
    except TypeError:
        print(4)
        app.logger.error('Argument `week_no` must be present in `{}`.'.format(request.path))
        abort(500)

    # Template name
    template_name = request.args.get('template_name') or SVG_DEFAULT_TEMPLATE
    if not template_name in list(get_export_templates('svg')):
        app.logger.error('`{}` is not a valid template for `{}`.'.format(template_name, request.path))
        abort(500)

    return year, week_no, template_name


@app.route('/', methods=['GET', 'POST'])
def submit_user_key_page():
    user_key = request.cookies.get('userkey')
    if user_key:
        is_valid = bool(check_user_key(user_key))
        if is_valid:
            url = UI_URL_TEMPLATE.format(user_key=user_key)
            return redirect(url, code=302)

    # When formed is filled out, redirect to dashboard
    if request.method == 'POST':
        user_key = request.form.get('userkey', None)
        if user_key:
            url = UI_URL_TEMPLATE.format(user_key=user_key)
            return redirect(url, code=302)

    # GET Request when page is loaded and page not submitted
    html = jinja2_env.get_template('login.html').render()
    response = make_response(html)
    return response


@app.route('/logout/')
def logout():
    response = make_response(redirect('/'))
    response.set_cookie('userkey', '', expires=0)
    return response


@app.route('/ui/<user_key>/')
def main_ui_page(user_key):
    user = check_user_key(user_key)
    if not user:
        abort(403)

    # GET Request when page is loaded and page not submitted
    context = {'user': user}
    html = jinja2_env.get_template('ui.html').render(context)
    response = make_response(html)
    response.set_cookie('userkey', user_key)
    return response


@app.route('/api/all')
def serve_json_all():
    user_key = request.cookies.get('userkey')
    user = check_user_key(user_key)
    if not user:
        abort(403)

    start_date = valid_date(request.args.get('startDate'), none_on_error=True)
    end_date = valid_date(request.args.get('endDate'), none_on_error=True)

    items = parse_docx(FILE_ID)
    json_data = serializers.get_validation_and_events(
        items, start_date=start_date, end_date=end_date)
    mimetype = 'application/json'

    response = make_response(json_data)
    response.headers['Content-Type'] = mimetype
    return response


@app.route('/api/events')
def serve_json_events():
    user_key = request.cookies.get('userkey')
    user = check_user_key(user_key)
    if not user:
        abort(403)

    start_date = valid_date(request.args.get('startDate'), none_on_error=True)
    end_date = valid_date(request.args.get('endDate'), none_on_error=True)

    items = parse_docx(FILE_ID)
    json_data = serializers.get_events(
        items, start_date=start_date, end_date=end_date)
    mimetype = 'application/json'

    response = make_response(json_data)
    response.headers['Content-Type'] = mimetype
    return response


@app.route('/api/validation')
def serve_json_validation():
    user_key = request.cookies.get('userkey')
    user = check_user_key(user_key)
    if not user:
        abort(403)

    items = parse_docx(FILE_ID)
    json_data = serializers.get_validation(items)
    mimetype = 'application/json'

    response = make_response(json_data)
    response.headers['Content-Type'] = mimetype
    return response


@app.route("/service/<user_key>/ical/")
def serve_ical(user_key):
    user = check_user_key(user_key)
    if not user:
        abort(403)

    start_date = valid_date(request.args.get('startDate'), none_on_error=True)
    end_date = valid_date(request.args.get('endDate'), none_on_error=True)

    items = parse_docx(FILE_ID)
    stream, mimetype = export.ical(
        items, start_date=start_date, end_date=end_date)

    response = make_response(stream.read())
    response.headers['Content-Type'] = mimetype
    response.headers['Content-Disposition'] = 'attachment; filename=calendar.ics'
    return response


@app.route("/service/<user_key>/programblad/docx")
def serve_programblad_docx(user_key):
    user = check_user_key(user_key)
    if not user:
        abort(403)

    start_date = valid_date(request.args.get('startDate'), none_on_error=True)
    end_date = valid_date(request.args.get('endDate'), none_on_error=True)

    items = parse_docx(FILE_ID)
    stream, mimetype = export.programblad_docx(
        items, start_date=start_date, end_date=end_date)

    response = make_response(stream.read())
    response.headers['Content-Type'] = mimetype
    response.headers['Content-Disposition'] = 'attachment; filename=programblad.docx'
    return response


@app.route("/service/<user_key>/by_event/csv")
def serve_by_events_csv(user_key):
    user = check_user_key(user_key)
    if not user:
        abort(403)

    start_date = valid_date(request.args.get('startDate'), none_on_error=True)
    end_date = valid_date(request.args.get('endDate'), none_on_error=True)

    items = parse_docx(FILE_ID)
    stream, mimetype = export.by_event_csv(
        items, start_date=start_date, end_date=end_date)

    response = make_response(stream.read())
    response.headers['Content-Type'] = mimetype
    response.headers['Content-Disposition'] = 'attachment; filename=calender.csv'
    return response


@app.route("/service/<user_key>/by_week/csv")
def serve_by_week_csv(user_key):
    user = check_user_key(user_key)
    if not user:
        abort(403)

    start_date = valid_date(request.args.get('startDate'), none_on_error=True)
    end_date = valid_date(request.args.get('endDate'), none_on_error=True)

    items = parse_docx(FILE_ID)
    stream, mimetype = export.by_week_csv(
        items, start_date=start_date, end_date=end_date)

    response = make_response(stream.read())
    response.headers['Content-Type'] = mimetype
    response.headers['Content-Disposition'] = 'attachment; filename=calender.csv'
    return response


@app.route("/service/<user_key>/week/svg")
def serve_week_svg(user_key):
    user = check_user_key(user_key)
    if not user:
        abort(403)

    year, week_no, template_name = _get_week_schedule_args(request)

    filename = '{}_V{}.svg'.format(year, week_no)

    items = parse_docx(FILE_ID)
    stream, mimetype = gen_week_schedule(items, year, week_no, template_name)

    response = make_response(stream.read())
    response.headers['Content-Type'] = mimetype
    response.headers['Content-Disposition'] = 'attachment; filename={}'.format(filename)
    return response


@app.route("/service/<user_key>/week/png")
def serve_week_png(user_key):
    user = check_user_key(user_key)
    if not user:
        abort(403)

    year, week_no, template_name = _get_week_schedule_args(request)

    filename = '{}_V{}.png'.format(year, week_no)

    items = parse_docx(FILE_ID)
    stream, mimetype = gen_week_schedule_png(items, year, week_no, template_name)

    response = make_response(stream.read())
    response.headers['Content-Type'] = mimetype
    response.headers['Content-Disposition'] = 'attachment; filename={}'.format(filename)
    return response


@app.route("/service/<user_key>/validate/plain_response")
def validate_plain_response(user_key):
    user = check_user_key(user_key)
    if not user:
        abort(403)

    items = parse_docx(FILE_ID)
    entries = run_all_checks(items)
    json = jsonify.plain(entries)

    response = make_response(json)
    response.headers['Content-Type'] = 'application/json'
    return response


@app.route("/service/<user_key>/validate/nested_response")
def validate_nested_response(user_key):
    user = check_user_key(user_key)
    if not user:
        abort(403)

    items = parse_docx(FILE_ID)
    entries = run_all_checks(items)
    json = jsonify.nested(entries)

    response = make_response(json)
    response.headers['Content-Type'] = 'application/json'
    return response
