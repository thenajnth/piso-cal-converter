# -*- coding: utf-8 -*-

from datetime import datetime, date

import os


def get_user_keys():
    def parse_raw_key(raw_key):
        splitted = raw_key.split('|')

        if len(splitted) >= 2:
            key = '|'.join(splitted[1:])
            desc = splitted[0]
            return key, desc

    keys = (parse_raw_key(os.environ[i])
            for i in os.environ
            if i.startswith('PISO_CAL_KEY'))

    return keys


def check_user_key(key):
    table = dict(get_user_keys())
    if key in table:
        return {'key': key, 'desc': table[key]}


def json_serial(obj):
    """
    JSON serializer for objects not serializable by default json code
    """

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError ("Type %s not serializable" % type(obj))
