# -*- coding: utf-8 -*-

from io import BytesIO
from urllib.request import urlopen

from piso_cal_converter.settings import DOC_URL_TEMPLATE


def get_docx_doc(file_id):
    url = DOC_URL_TEMPLATE.format(file_id=file_id, fmt='doc')

    with urlopen(url) as response:
        doc = BytesIO(response.read())

    return doc
