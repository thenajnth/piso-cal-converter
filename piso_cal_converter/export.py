# -*- coding: utf-8 -*-

import csv
import hashlib

from datetime import datetime, date
from io import BytesIO, StringIO

from icalendar import Calendar as IcalCalendar
from icalendar import Event as IcalEvent
from isoweek import Week
from docx import Document
from docx.enum.style import WD_STYLE_TYPE
from docx.shared import Cm, Pt, Mm, RGBColor

from piso_cal_converter import settings
from piso_cal_converter.locales import LocalizedDatetime
from piso_cal_converter.tools import events_by_date, events_by_weekno, get_events_from_items


def programblad_docx(items, start_date=None, end_date=None, pagesize='A5'):
    def set_page_a5(document):
        first_section = document.sections[0]
        first_section.page_height = Mm(210)
        first_section.page_width = Mm(148.5)

        # Set margins
        sections = document.sections
        margin = 13
        for section in sections:
            section.top_margin = Mm(margin)
            section.bottom_margin = Mm(margin)
            section.left_margin = Mm(margin)
            section.right_margin = Mm(margin)

        return document

    def set_page_a4(document):
        # Set margins
        sections = document.sections
        margin_top_bottom = 20
        margin_left_right = 40
        for section in sections:
            section.top_margin = Mm(margin_top_bottom)
            section.bottom_margin = Mm(margin_top_bottom)
            section.left_margin = Mm(margin_left_right)
            section.right_margin = Mm(margin_left_right)

        return document

    def set_doc_style(document):
        styles = document.styles

        # Normal
        normal = styles['Normal']
        normal.font.name = 'Myriad Pro'
        normal.font.size = Pt(10)
        normal.paragraph_format.space_after = 0

        # Info text
        infotext = styles.add_style('Infotext', WD_STYLE_TYPE.PARAGRAPH)
        infotext.base_style = styles['Heading 1']
        infotext.font.bold = False
        infotext.font.italic = True
        infotext.font.name = 'Myriad Pro'
        infotext.font.size = Pt(12)
        infotext.font.color.rgb = RGBColor(0x00, 0x00, 0x00)

        # Week
        week = styles.add_style('Vecka', WD_STYLE_TYPE.PARAGRAPH)
        week.base_style = styles['Heading 2']
        week.font.name = 'Myriad Pro'
        week.font.size = Pt(8)
        week.font.color.rgb = RGBColor(0x98, 0x98, 0x9c)

        # Date and time
        dt = styles.add_style('Datum och tid', WD_STYLE_TYPE.PARAGRAPH)
        dt.base_style = styles['Heading 3']
        dt.font.name = 'Myriad Pro'
        dt.font.size = Pt(10)
        dt.font.color.rgb = RGBColor(0x00, 0x00, 0x00)

        return document

    events = get_events_from_items(
        items, start_date=start_date, end_date=end_date)

    document = Document()

    if pagesize == 'A4':
        document = set_page_a4(document)
    elif pagesize == 'A5':
        document = set_page_a5(document)
    else:
        raise TypeError("variable pagesize must be 'A5' or 'A4'.")

    document = set_doc_style(document)

    # Set infoheader
    infotext = '{} till {}'.format(
        start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d'))
    document.add_paragraph(infotext, style='Infotext')

    for weekno, events_in_week in events_by_weekno(events).items():
        # Add heading for this week
        text = 'VECKA {}'.format(weekno)
        document.add_paragraph(text, style='Vecka')

        # Iterate all dates belongning to this week
        for date_str, events_this_day in events_by_date(events_in_week).items():
            start = datetime.strptime(date_str, '%Y-%m-%d')
            localized = LocalizedDatetime(start, locale=settings.LOCALE)
            text = localized.style1_date.upper()
            document.add_paragraph(text, style='Datum och tid')

            # Iterate all events for this day
            for event in events_this_day:
                p = document.add_paragraph(style='Normal')
                tab_stops = p.paragraph_format.tab_stops
                tab_stops.add_tab_stop(Cm(1.8))

                # Only add time if event is datetime and not date
                if hasattr(event.start, 'time'):
                    timestamp = event.start.strftime('%H:%M')
                    p.add_run(timestamp).bold = True

                p.add_run('\t' + event.desc)

    stream = BytesIO()
    document.save(stream)
    stream.seek(0)

    mimetype = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    return stream, mimetype


def by_event_csv(items, start_date=None, end_date=None):
    def make_rows(items):
        yield ['sep=,']
        yield ['title', 'startdate', 'enddate', 'starttime', 'location',
               'content', "category_slugs"]

        for item in items:
            title = item.desc
            start_date = item.start.strftime('%Y-%m-%d')
            end_date = ''
            time = item.start.strftime('%H:%M')
            location = ''
            details = ''
            yield [title, start_date, end_date, time, location, details, '']

    events = get_events_from_items(
        items, start_date=start_date, end_date=end_date)

    stream = StringIO()
    writer = csv.writer(stream, delimiter=',', quoting=csv.QUOTE_ALL)
    for row in make_rows(events):
        writer.writerow(row)
    stream.seek(0)

    mimetype = 'text/csv'
    stream = BytesIO(stream.read().encode('utf-8'))  # StringIO to BytesIO
    stream.seek(0)
    return stream, mimetype


def by_week_csv(items, start_date=None, end_date=None):
    def make_rows(items):
        yield ['sep=,']
        yield ['title', 'startdate', 'enddate', 'starttime', 'location',
               'content', "category_slugs"]

        for weekno, events in events_by_weekno(items).items():
            title = 'Vecka {}'.format(weekno)
            weekobj = Week.withdate(events[0].start)
            start_date = weekobj.monday().strftime('%Y-%m-%d')
            end_date = weekobj.sunday().strftime('%Y-%m-%d')

            # Create HTML-string with all events this week
            details = ''
            for event in events:
                localized = LocalizedDatetime(
                    event.start, locale=settings.LOCALE)
                details += '<p><strong>{}</strong><br/>{}</p>'.format(
                    localized.style1_datetime, event.desc)

            yield [title, start_date, end_date, '', '', details, '']

    events = get_events_from_items(
        items, start_date=start_date, end_date=end_date)

    # Create in memory csv
    stream = StringIO()
    writer = csv.writer(stream, delimiter=',', quoting=csv.QUOTE_ALL)
    for row in make_rows(events):
        writer.writerow(row)
    stream.seek(0)

    mimetype = 'text/csv'
    stream = BytesIO(stream.read().encode('utf-8'))  # StringIO to BytesIO
    stream.seek(0)
    return stream, mimetype


def ical(items, start_date=None, end_date=None):
    def create_uid(dtstart, desc):
        # Get timestring
        iso8601 = dtstart.strftime('%Y%m%dT%H%M%SZ')

        # Create hash
        m = hashlib.md5()
        m.update(dtstart.isoformat().encode('utf-8'))
        m.update(desc.encode('utf-8'))
        hashstr = m.hexdigest().upper()

        domain = 'www.soderkoping.pingst.se'

        return '{}-{}@{}'.format(iso8601, hashstr, domain)

    events = get_events_from_items(
        items, start_date=start_date, end_date=end_date)

    ical_cal = IcalCalendar()

    ical_cal.add('prodid', '-//Pingst Calendar generator//theninth.se//')
    ical_cal.add('version', '0.1')

    for event in events:
        ical_event = IcalEvent()
        ical_event.add('uid', create_uid(event.start, event.desc))
        ical_event.add('summary', event.desc)
        ical_event.add('dtstart', event.start_localized)
        ical_event.add('status', 'confirmed'.upper())
        ical_cal.add_component(ical_event)

    data = ical_cal.to_ical()

    stream = BytesIO(data)
    mimetype = 'text/calendar'
    return stream, mimetype
