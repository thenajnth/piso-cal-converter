import json
import sys

from os.path import join

from piso_cal_converter.image_export.svg_tools import gen_style_str
from piso_cal_converter.settings import EXPORT_TEMPLATES_DIR


class ExportTemplate(object):
    def __init__(self, template_type, template_name):
        def get_template_full_path(template_type, template_name):
            if template_type not in ['svg', 'video']:
                raise TypeError("template_name must be 'svg' or 'video'")
            path_of_type = join(EXPORT_TEMPLATES_DIR, template_type)
            template_path = join(path_of_type, template_name)
            return template_path

        self.name = template_name
        self.template_path = get_template_full_path(
            template_type, template_name)
        self._config = self.parse_config(self.template_path)

        try:
            self._styles = self._config['styles']
        except KeyError:
            print('ERROR: Template "{}" is missing key "styles" in config file.'.format(
                template_name))
            sys.exit(1)

        try:
            self._svg = self._config['svg']
        except KeyError:
            print('ERROR: Template "{}" is missing key "svg" in config file.'.format(
                template_name))
            sys.exit(1)

    def get_style(self, name):
        return self._styles[name]

    def get_style_str(self, name):
        return gen_style_str(self.get_style(name))

    def parse_config(self, template_path):
        config_path = join(template_path, 'config.json')
        with open(config_path, 'r') as f:
            return json.load(f)

    @property
    def svg_fullpath(self):
        return join(self.template_path, self._svg)


class VideoTemplate(ExportTemplate):
    def __init__(self, template_name):
        template_type = 'video'
        super().__init__(template_type, template_name)

        try:
            self._video = self._config['video']
        except KeyError:
            print('ERROR: Template "{}" is missing key "video" in config file.'.format(
                template_name))
            sys.exit(1)

        try:
            self._svg = self._config['svg']
        except KeyError:
            print('ERROR: Template "{}" is missing key "overlay" in config file.'.format(
                template_name))
            sys.exit(1)

    @property
    def video_fullpath(self):
        return join(self.template_path, self._video)


class SvgTemplate(ExportTemplate):
    def __init__(self, template_name):
        template_type = 'svg'
        super().__init__(template_type, template_name)

        try:
            self._styles = self._config['styles']
        except KeyError:
            print('ERROR: Template "{}" is missing key "styles" in config file.'.format(
                template_name))
            sys.exit(1)
