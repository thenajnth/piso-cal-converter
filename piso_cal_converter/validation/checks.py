# -*- coding: utf-8 -*-

import datetime
from isoweek import Week

from piso_cal_converter.baseclasses import Event, WeekDesc
from piso_cal_converter.validation.classes import ValidationError
from piso_cal_converter.validation.classes import ValidationInfo
from piso_cal_converter.validation.classes import ValidationWarning


def week_numbering(items):
    entries = []
    last_week_no = None
    warned_about_events_before_weekno = False

    for item in items:
        if isinstance(item, Event):
            if not last_week_no:
                if not warned_about_events_before_weekno:
                    entry = ValidationWarning(
                        'Händelse i kalendern hittades före någon vecka skrivits ut'
                    )
                    entries.append(entry)
                    warned_about_events_before_weekno = True
                continue

            real_week_no = Week.withdate(item.start).week
            if real_week_no != last_week_no:
                error_text = '"{}" påstås ligga i vecka {}, men i verkligheten är det datumet i vecka {}'
                entry = ValidationError(error_text.format(item, last_week_no, real_week_no))
                entries.append(entry)

        elif isinstance(item, WeekDesc):
            last_week_no = item.week_no

    return entries


def provided_day_name(items):
    entries = []

    events = [item
              for item in items
              if isinstance(item, Event)]

    def parse_swe_day_name(day_name):
        swe_day_names = {
            'mån':     1,
            'månd':    1,
            'måndag':  1,
            'tis':     2,
            'tisd':    2,
            'tisdag':  2,
            'ons':     3,
            'onsd':    3,
            'onsdag':  3,
            'tor':     4,
            'tors':    4,
            'torsdag': 4,
            'fre':     5,
            'fred':    5,
            'fredag':  5,
            'lör':     6,
            'lörd':    6,
            'lördag':  6,
            'sön':     0,
            'sönd':    0,
            'söndag':  0
        }
        return swe_day_names.get(day_name.lower())

    for event in events:
        provided_day_as_no = parse_swe_day_name(event.raw_data['day_name'])
        if not event.raw_data['day_name'].strip():
            continue
        elif int(event.start.strftime('%w')) != provided_day_as_no:
            error_text = 'Dag som angavs för "{}" var "{}" men händelsen ligger på en {}.'
            entry = ValidationError(error_text.format(event, event.raw_data['day_name'],
                                                      event.start.strftime('%A')))
            entries.append(entry)

    return entries


def date_order(items):   
    entries = []
    last_event = None

    events = [item
              for item in items
              if isinstance(item, Event)]

    for event in events:
        if not last_event:
            last_event = event
            continue

        if not isinstance(last_event.start, datetime.datetime) and isinstance(event.start, datetime.datetime):
            if last_event.start > event.start.date():
                error_text = '"{}" ligger tidigare än "{}" trots att den första har ett senare datum.'
                entry = ValidationError(error_text.format(last_event, event))
                entries.append(entry)
        elif isinstance(last_event.start, datetime.datetime) and not isinstance(event.start, datetime.datetime):
            if last_event.start.date() > event.start:
                error_text = '"{}" ligger tidigare än "{}" trots att den första har ett senare datum.'
                entry = ValidationError(error_text.format(last_event, event))
                entries.append(entry)
        else:
            if last_event.start > event.start:
                error_text = '"{}" ligger tidigare än "{}" trots att den första har ett senare datum.'
                entry = ValidationError(error_text.format(last_event, event))
                entries.append(entry)

        last_event = event

    return entries


def run_all(items):
    entries = []
    entries += week_numbering(items)
    entries += date_order(items)
    entries += provided_day_name(items)

    return entries
