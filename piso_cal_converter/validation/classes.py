class ValidationBase():
    def __init__(self, text):
        self.bs_class = ''
        self.severity = None
        self.text = text

    def __str__(self):
        return '{}: {}'.format(self.severity.capitalize(), self.text)

    def get_dict(self):
        """Returns class as python dict ready to be serialized"""
        return vars(self)


class ValidationError(ValidationBase):
    def __init__(self, *args, **kwargs):
        super(ValidationError, self).__init__(*args, **kwargs)
        self.bs_class = 'alert-warning'
        self.severity = 'fel'


class ValidationInfo(ValidationBase):
    def __init__(self, *args, **kwargs):
        super(ValidationInfo, self).__init__(*args, **kwargs)
        self.bs_class = 'alert-info'
        self.severity = 'info'


class ValidationWarning(ValidationBase):
    def __init__(self, *args, **kwargs):
        super(ValidationWarning, self).__init__(*args, **kwargs)
        self.bs_class = 'alert-danger'
        self.severity = 'varning'
