import json


def plain(entries):
    return json.dumps([vars(e) for e in entries])


def nested(entries):
    def purge_severity(d):
        return {k: v for k, v in d.items() if k != 'severity'}

    by_severity = {}
    for entry in entries:
        if entry.severity in by_severity:
            by_severity[entry.severity].append(purge_severity(vars(entry)))
        else:
            by_severity[entry.severity] = [purge_severity(vars(entry))]

    return json.dumps(by_severity)
