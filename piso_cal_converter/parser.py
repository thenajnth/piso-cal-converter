# -*- coding: utf-8 -*-

import re

import docx

from piso_cal_converter.importer import get_docx_doc
from piso_cal_converter.baseclasses import Event, WeekDesc

def _is_paragraph_event(paragraph):
    tabbed = paragraph.split('\t')

    # Check length
    if len(tabbed) < 4:
        return False

    # Check if second col is date
    date_col = tabbed[1]
    if not re.match(r'^\d{4}-\d\d-\d\d', date_col):
        return False

    # If no chech returned False, return True
    return True


def _is_paragraph_week_desc(paragraph):
    pattern = r'^\s*vecka\s+(\d+)\s*(.*)'
    match = re.match(pattern, paragraph, re.IGNORECASE)
    return bool(match)


def parse_docx(file_id):
    doc = docx.Document(get_docx_doc(file_id))

    items = []

    for p in doc.paragraphs:
        if _is_paragraph_event(p.text):
            tabbed = p.text.split('\t')

            provided_day_name = tabbed[0]
            date = tabbed[1]
            time = tabbed[2]
            desc = ' '.join(tabbed[3:])  # Merge all tabs with space

            event = Event(provided_day_name, date, time, desc)
            items.append(event)
        elif _is_paragraph_week_desc(p.text):
            items.append(WeekDesc(p.text))

    return items
