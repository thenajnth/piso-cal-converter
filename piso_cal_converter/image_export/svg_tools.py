def gen_style_str(style):
    """
    Takes data as a dictonary like this:

        {
            'font-style': 'normal',
            'font-variant': 'normal',
            'font-weight': '300',
            'font-size': '42.7px',
            'font-family': "'IBM Plex Sans'"
        }

    And returns a string that looks like this:

        "font-style:normal;font-variant:normal;font-weight:300;font-size:42.7px;font-family:'IBM Plex Sans'"

    """

    return ';'.join(['{}:{}'.format(k, v) for k, v in style.items()])
