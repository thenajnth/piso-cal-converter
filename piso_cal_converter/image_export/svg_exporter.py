import os.path
import subprocess
import tempfile
import xml.etree.ElementTree as ET

from datetime import datetime
from io import BytesIO
from isoweek import Week

from piso_cal_converter import settings
from piso_cal_converter.constants import SVG_FLOWROOT_SCHEDULE_ID
from piso_cal_converter.constants import SVG_NAMESPACES
from piso_cal_converter.constants import SVG_TSPAN_YEAR_ID
from piso_cal_converter.constants import SVG_TSPAN_WEEK_ID
from piso_cal_converter.locales import LocalizedDatetime
from piso_cal_converter.tools import events_by_date
from piso_cal_converter.tools import get_events_from_items
from piso_cal_converter.tools import get_export_templates


class SvgExporter:
    def __init__(self, template):
        self._flow_para_schedule_cntr = 0
        self._flow_span_schedule_cntr = 0

        self.template = template
        self.tree = ET.parse(self.template.svg_fullpath)

        # Unregistered namespaces will create XML-parser warnings
        for prefix, uri in SVG_NAMESPACES.items():
            ET.register_namespace(prefix, uri)

        self.clear_schedule_text()

    def _flow_para_schedule_unique_id(self):
        unique_id = 'flowParaSchedule{0:03d}'.format(
            self._flow_para_schedule_cntr)
        self._flow_para_schedule_cntr += 1
        return unique_id

    def _flow_span_schedule_unique_id(self):
        unique_id = 'flowSpanSchedule{0:03d}'.format(
            self._flow_span_schedule_cntr)
        self._flow_span_schedule_cntr += 1
        return unique_id

    def add_date(self, text, style):
        textbox = self.tree.find(
            './/svg:flowRoot[@id="{}"]'.format(SVG_FLOWROOT_SCHEDULE_ID), SVG_NAMESPACES)
        paragraph = ET.SubElement(textbox, 'svg:flowPara', SVG_NAMESPACES)
        paragraph.set('style', style)
        paragraph.text = text

    def add_empty_paragraph(self):
        textbox = self.tree.find(
            './/svg:flowRoot[@id="{}"]'.format(SVG_FLOWROOT_SCHEDULE_ID), SVG_NAMESPACES)
        paragraph = ET.SubElement(textbox, 'svg:flowPara', SVG_NAMESPACES)

    def add_event(self, timestr, desc, style_time, style_desc):
        textbox = self.tree.find(
            './/svg:flowRoot[@id="{}"]'.format(SVG_FLOWROOT_SCHEDULE_ID), SVG_NAMESPACES)

        paragraph = ET.SubElement(textbox, 'svg:flowPara', SVG_NAMESPACES)

        timeentry = ET.SubElement(paragraph, 'svg:flowSpan', SVG_NAMESPACES)
        timeentry.set('id', self._flow_span_schedule_unique_id())
        timeentry.set('style', style_time)

        descentry = ET.SubElement(paragraph, 'svg:flowSpan', SVG_NAMESPACES)
        descentry.set('id', self._flow_span_schedule_unique_id())
        descentry.set('style', style_desc)

        timeentry.text = timestr

        if timestr:  # Add space only when time is present
            descentry.text = ' ' + desc
        else:
            descentry.text = desc

    def clear_schedule_text(self):
        textbox = self.tree.find(
            './/svg:flowRoot[@id="{}"]'.format(SVG_FLOWROOT_SCHEDULE_ID), SVG_NAMESPACES)
        elements = textbox.findall('svg:flowPara', SVG_NAMESPACES)
        for element in elements:
            textbox.remove(element)

    def generate(self):
        return ET.tostring(self.tree.getroot(), encoding='utf8', method='xml')

    def update_week(self, text):
        tspan = self.tree.find(
            './/svg:tspan[@id="' + SVG_TSPAN_WEEK_ID + '"]', SVG_NAMESPACES)
        try:
            tspan.text = text
        except AttributeError:
            pass  # Week info is probably not available in template

    def update_year(self, text):
        tspan = self.tree.find(
            './/svg:tspan[@id="' + SVG_TSPAN_YEAR_ID + '"]', SVG_NAMESPACES)
        try:
            tspan.text = text
        except AttributeError:
            pass  # Year info is probably not available in template


def gen_week_schedule(items, year, week_no, template_name, template_cat):
    assert template_cat in ['svg', 'video']

    _templates = get_export_templates(template_cat)
    template = _templates[template_name]

    week = Week(year, week_no)
    events = get_events_from_items(
        items, start_date=week.monday(), end_date=week.sunday())

    exporter = SvgExporter(template)

    for date_str, events in events_by_date(events).items():
        localized = LocalizedDatetime(datetime.strptime(date_str, '%Y-%m-%d'))

        exporter.add_date(localized.style1_date,
                          exporter.template.get_style_str('text_h1_style'))

        for event in events:
            exporter.add_event(event.get_dict()['start_time_readable'], event.desc,
                               exporter.template.get_style_str('time_style'),
                               exporter.template.get_style_str('text_standard_style'))

        exporter.add_empty_paragraph()

    exporter.update_year(settings.SVG_YEAR_TEMPLATE.format(year))
    exporter.update_week(settings.SVG_WEEK_TEMPLATE.format(week_no))

    stream = BytesIO(exporter.generate())
    mimetype = 'image/svg+xml'
    return stream, mimetype


def gen_week_schedule_png(items, year, week_no, template_name, template_cat):
    stream, _ = gen_week_schedule(
        items, year, week_no, template_name, template_cat)

    f_svg = tempfile.NamedTemporaryFile(
        mode='w+b', suffix='.svg', prefix='piso_cal_export_svg_')
    f_svg.write(stream.read())

    f_png = tempfile.NamedTemporaryFile(
        mode='r+b', suffix='.png', prefix='piso_cal_export_svg_')

    # Inkscape 0.92
    # subprocess.run(['inkscape', '-z', f_svg.name, '-e', f_png.name, ''])

    # Inscape 1.0
    subprocess.run(['inkscape', '--export-type=png', f_svg.name,
                    '--export-filename={}'.format(f_png.name),
                    '--batch-process'])
    f_svg.close()

    stream = BytesIO(f_png.read())
    f_png.close()
    mimetype = 'image/png'

    return stream, mimetype
