# -*- coding: utf-8 -*-

from datetime import datetime
import json
import re
import sys

from os.path import dirname, join

from isoweek import Week

from piso_cal_converter.image_export.svg_tools import gen_style_str
from piso_cal_converter.locales import LocalizedDatetime
from piso_cal_converter.settings import TZ, EXPORT_TEMPLATES_DIR


class Event:
    def __init__(self, provided_day_name, date, time, desc):
        self.category = 'event'
        self.raw_data = {}
        self.raw_data['day_name'] = provided_day_name
        self.raw_data['date'] = date
        self.raw_data['time'] = time
        self.raw_data['desc'] = desc
        self.start = self._raw_data_to_datetime()

    def __str__(self):
        def _shorten_text(text, max_length=15):
            if len(text) <= max_length:
                return text
            return text[:max_length] + '...'

        localized = LocalizedDatetime(self.start)
        return '{} {}'.format(localized.style1_datetime, _shorten_text(self.desc))

    def _raw_data_to_datetime(self):
        if self.raw_data['time']:
            date_str = '{}T{}'.format(
                self.raw_data['date'], self.raw_data['time'])
            return datetime.strptime(date_str, '%Y-%m-%dT%H:%M')

        return datetime.strptime(self.raw_data['date'], '%Y-%m-%d').date()

    def get_dict(self):
        """Returns class as python dict ready to be serialized"""
        def datetime_to_readable_date(obj):
            if isinstance(obj, datetime):
                return LocalizedDatetime(obj.date()).style1_date
            return LocalizedDatetime(obj).style1_date

        def datetime_to_readable_time(obj):
            if isinstance(obj, datetime):
                return obj.strftime('%H:%M')

        data = {
            'desc': self.desc,
            'start': self.start,
            'start_date_readable': datetime_to_readable_date(self.start),
            'start_time_readable': datetime_to_readable_time(self.start),
            'weekno': self.weekno
        }
        return data

    @property
    def desc(self):
        return self.raw_data.get('desc')

    @property
    def start_localized(self):
        if isinstance(self.start, datetime):
            localized = TZ.localize(self.start)
            return localized

        return self.start

    @property
    def weekno(self):
        _week = Week.withdate(self.start)
        return _week.week


class WeekDesc:
    def __init__(self, week_text_string):
        def extract_data(week_text_string):
            pattern = '^\s*vecka\s+(\d+)\s*(.*)'
            m = re.match(pattern, week_text_string, re.IGNORECASE)
            if m:
                week_no = int(m.group(1))
                desc = m.group(2) or None
                return (week_no, desc)
            else:
                raise ValueError

        self.week_no, self.desc = extract_data(week_text_string)

    def __str__(self):
        if self.desc:
            return '{} ({})'.format(self.week_no, self.desc)
        return str(self.week_no)
