#!/usr/bin/env python3

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open('requirements.txt') as fp:
    install_requires = fp.read()

setuptools.setup(
    name="piso-cal-converter",
    version="0.0.1",
    author="Niclas Nilsson",
    author_email="niclas.k.nilsson@gmail.com",
    description="Tools for managing planning doc for Pingstkyrkan Söderköping",
    install_requires=install_requires,
    long_description=long_description,
    long_description_content_type="text/markdown",
    scripts=[
        'bin/piso-cal-conv',
        'bin/piso-cal-week-gen',
        'bin/piso-cal-export-svg',
        'bin/piso-cal-export-video'
    ],
    url="https://bitbucket.org/thenajnth/piso-cal-converter/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
