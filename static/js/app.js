Vue.config.devtools = true

// https://stackoverflow.com/questions/563406/add-days-to-javascript-date
Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

// https://stackoverflow.com/questions/5639346/
// what-is-the-shortest-function-for-reading-a-cookie-by-name-in-javascript
function getCookieValue(a) {
    var b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
    return b ? b.pop() : '';
}

var emptyDataModel = {events: [], validation: []};
var initStartDate = new Date().toISOString().substring(0, 10);
var initEndDate = new Date().addDays(90).toISOString().substring(0, 10);


var app = new Vue({
    el: '#app',
    data: {
	data: emptyDataModel,
	startDate: initStartDate,
	endDate: initEndDate,
	userkey: getCookieValue('userkey')
    },
    created: function () {
	// this.fetchData();
    },
    watch: {
	startDate: function (val, oldVal) {
	    // this.fetchData();
	},
	endDate: function (val, oldVal) {
	    // this.fetchData();
	}
    },
    methods: {
	fetchData: function() {
	    var _this = this;
	    $.getJSON('/api/all?startDate=' + this.startDate +
		      '&endDate=' + this.endDate,
		      function (json) {
			  _this.data = json;
		      });
	}
    }
})
