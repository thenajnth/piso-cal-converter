#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys
from datetime import datetime

from piso_cal_converter.parser import parse_docx
from piso_cal_converter.tools import valid_date
from piso_cal_converter.validation import run_all_checks
from piso_cal_converter import export


def validate_input_doc(items):
    entries = run_all_checks(items)
    if entries:
        print('\nFöljande problem hittades i grunddokumentet:')
        for e in entries:
            print('  - {}'.format(e))


def main():
    timestamp_start = datetime.now().strftime('%Y-%m-%d_%H_%M')

    parser = argparse.ArgumentParser(description=('Konvertera mitt Google dokument ' +
                                                  'för planering till andra format.'))
    parser.add_argument(
        '-i', '--fileid', help='Google Docs file-id på in-filen')
    parser.add_argument('-f', '--format',
                        choices=[
                            'by_week_csv', 'by_event_csv',
                            'programblad_docx_a5',
                            'programblad_docx_a4', 'ical'
                        ],
                        default='by_week_csv', help='ut-format')
    parser.add_argument('-o', '--output_path', help='filename to write to')
    parser.add_argument('-s', '--start_date', type=valid_date,
                        help='Startdatum som YYYY-MM-DD')
    parser.add_argument('-e', '--end_date', type=valid_date,
                        help='Slutdatum som YYYY-MM-DD')

    args = parser.parse_args()

    # Get file_id from CLI argument or environment variable
    file_id = args.fileid or os.environ.get('PLANNING_FILE_ID')
    if not file_id:
        text = ('FEL: Scriptet behöver ett Google Docs file-id, antingen via scriptets argument ' +
                '`file_id` eller via miljövariabeln `PLANNING_FILE_ID`')
        print('\n\n' + text)
        sys.exit(2)

    # Get items from google docs (via the docx-format)
    items = parse_docx(file_id)

    # Start some checks to warn if something is not looking right.
    validate_input_doc(items)

    if args.format == 'by_week_csv':
        output_path = args.output_path or timestamp_start + '_by_weeks.csv'
        stream, _ = export.by_week_csv(
            items, start_date=args.start_date, end_date=args.end_date)
        with open(output_path, 'wb') as f:
            f.write(stream.read())

    elif args.format == 'by_event_csv':
        output_path = args.output_path or timestamp_start + '_by_events.csv'
        stream, _ = export.by_event_csv(
            items, start_date=args.start_date, end_date=args.end_date)
        with open(output_path, 'wb') as f:
            f.write(stream.read())

    elif args.format == 'programblad_docx_a5':
        output_path = args.output_path or timestamp_start + '_programblad.docx'
        stream, _ = export.programblad_docx(items, start_date=args.start_date,
                                            end_date=args.end_date, pagesize='A5')
        with open(output_path, 'wb') as f:
            f.write(stream.read())

    elif args.format == 'programblad_docx_a4':
        output_path = args.output_path or timestamp_start + '_programblad.docx'
        stream, _ = export.programblad_docx(items, start_date=args.start_date,
                                            end_date=args.end_date, pagesize='A4')
        with open(output_path, 'wb') as f:
            f.write(stream.read())

    elif args.format == 'ical':
        output_path = args.output_path or timestamp_start + '_pingstkyrkan_skpg.ics'
        stream, _ = export.ical(
            items, start_date=args.start_date, end_date=args.end_date)
        with open(output_path, 'wb') as f:
            f.write(stream.read())

    print('\nSkrev till fil: {}.'.format(output_path))


if __name__ == '__main__':
    main()
